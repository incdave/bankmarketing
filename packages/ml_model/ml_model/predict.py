import pandas as pd
import ml_model.config as config
import joblib
from ml_model import __version__ as _version

import logging
_logger = logging.getLogger(__name__)

model_path = config.TRAINED_MODEL_DIR + config.MODEL_NAME + _version + ".pkl"
treshold_path = config.TRAINED_MODEL_DIR + config.THRESHOLD_NAME + _version + ".pkl"
treshold = joblib.load(treshold_path)
clf = joblib.load(model_path)

def run_prediction(input_model):
	input_model = pd.read_json(input_model)
	prob = clf.predict_proba(input_model)[:,1]
	pred = (prob > treshold)*1
	response = {"prediction": pred.tolist(), "probability": prob.tolist(), "version": _version}

	_logger.info(
        f"Making predictions with model version: {_version} "
        f"Inputs: {input_model.head(10)} "
        f"Predictions: {response.get('prediction')[0:10]}"
    )

	return response