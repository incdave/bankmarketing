import ml_model.helper as helper
import ml_model.config as config
import pandas as pd
import ml_model.preprocessing as pre
import category_encoders as ce
from sklearn.pipeline import FeatureUnion
from imblearn.over_sampling import SMOTE 
from imblearn.pipeline import make_pipeline
import lightgbm as lgb
import joblib
from ml_model import __version__ as _version
from sklearn.linear_model import LogisticRegression
import argparse

import logging
_logger = logging.getLogger(__name__)
global circleci_bool

parser = argparse.ArgumentParser()
parser.add_argument('-ci', '--circleci', action='store_true', help="use different model for circleci which is less compute power")
args = parser.parse_args()
circleci_bool = args.circleci

def save_model(fitted_model):
    """Persist the model."""
    model_path = config.TRAINED_MODEL_DIR + config.MODEL_NAME + _version + ".pkl"
    joblib.dump(fitted_model, model_path)
    _logger.info(f"save model version: {_version}")

def save_threshold(threshold):
    treshold_path = config.TRAINED_MODEL_DIR + config.THRESHOLD_NAME + _version + ".pkl"
    joblib.dump(threshold, treshold_path)
    _logger.info(f"save threshold version: {_version}")

def run_train():
    """Train the model."""

    helper.download_files(config.SAVE_PATH, config.LINK)
    helper.save_files()

    X_train, y_train = joblib.load(config.DATA_DIR + config.TRAIN)

    smt = SMOTE(random_state = config.RANDOM)
    if not circleci_bool:
        clf = lgb.LGBMClassifier(boosting_type='gbdt', class_weight=None, colsample_bytree=1.0,
                   importance_type='split', learning_rate=0.1, max_depth=-1,
                   min_child_samples=20, min_child_weight=0.001, min_split_gain=0.0,
                   n_estimators=100, n_jobs=-1, num_leaves=31, objective=None,
                   random_state=config.RANDOM, reg_alpha=0.0, reg_lambda=0.0, silent=False,
                   subsample=1.0, subsample_for_bin=200000, subsample_freq=0)
    else:
        clf = LogisticRegression()

    pipeline = make_pipeline(pre.AgeSplit, pre.DropFeatures(["duration"]),FeatureUnion([("jse", ce.JamesSteinEncoder(cols=config.COLTRANSFORM)), 
        ("ohe", ce.OneHotEncoder(cols=config.COLTRANSFORM))]), smt, clf)
    fitted_model = pipeline.fit(X_train, y_train.values.ravel())
    
    save_model(fitted_model)
    threshold = helper.calculate_best_threshold(y_train, pipeline.predict_proba(X_train)[:,1])
    save_threshold(threshold)

if __name__ == "__main__":
    run_train()