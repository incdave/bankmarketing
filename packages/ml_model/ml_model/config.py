import pathlib
import ml_model

PACKAGE_ROOT = str(pathlib.Path(ml_model.__file__).resolve().parent)
RANDOM = 123
SAVE_PATH = PACKAGE_ROOT + "/data/bankmarketing.zip"
LINK = "https://archive.ics.uci.edu/ml/machine-learning-databases/00222/bank-additional.zip"
ZIP_OPEN = 'bank-additional/bank-additional-full.csv'
COLTRANSFORM = ['job', 'marital', 'education', 'default', 'housing', 'loan', 'contact', 'month', 'day_of_week', 'poutcome']
TRAINED_MODEL_DIR = PACKAGE_ROOT + "/model/"
DATA_DIR = PACKAGE_ROOT + "/data/"
MODEL_NAME = "fitted_model"
THRESHOLD_NAME = "threshold"
TRAIN = "trained_data.pkl"
TEST = "test_data.pkl"