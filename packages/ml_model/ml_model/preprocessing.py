from sklearn.preprocessing import FunctionTransformer
from sklearn.base import BaseEstimator, TransformerMixin

def age_split(X):
	# Avoids SettingWithCopyWarning
	X = X.copy()
	X["age_st21"] = (X.loc[:,"age"] < 21)*1
	X["age_st60"] = (X.loc[:,"age"] > 60)*1
	return X

AgeSplit = FunctionTransformer(age_split)

class DropFeatures(BaseEstimator, TransformerMixin):
    def __init__(self, variables_to_drop=None):
        self.variables = variables_to_drop

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        # encode labels
        X = X.copy()
        X = X.drop(self.variables, axis=1)

        return X