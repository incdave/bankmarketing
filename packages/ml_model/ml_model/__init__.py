import logging
import sys
import ml_model.config as config


FORMATTER = logging.Formatter(
    "%(asctime)s — %(name)s — %(levelname)s —" "%(funcName)s:%(lineno)d — %(message)s"
)
console_handler = logging.StreamHandler(sys.stdout)
console_handler.setFormatter(FORMATTER)


VERSION_PATH = config.PACKAGE_ROOT + '/VERSION'

# Configure logger for use in package
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.addHandler(console_handler)
logger.propagate = False


with open(VERSION_PATH, 'r') as version_file:
    __version__ = version_file.read().strip()