from sklearn.metrics import precision_recall_curve, accuracy_score, recall_score, precision_score
import numpy as np
import urllib.request
import config
import os
import joblib
from zipfile import ZipFile
from sklearn.model_selection import train_test_split
import pandas as pd

def download_files(save_path, link):
    if not os.path.exists(save_path):
        urllib.request.urlretrieve(link, save_path)

def calculate_best_threshold(y, y_pred, f=1):
    precision, recall, thresholds = precision_recall_curve(y, y_pred)
    f_scores = (1+f**2)*recall*precision/((f**2*recall)+precision)
    print(f_scores)
    print('Best threshold: ', thresholds[np.argmax(f_scores)])
    print('Best F-Score: ', np.max(f_scores))
    print('Recall: ', recall[np.argmax(f_scores)])
    print('Precision: ', precision[np.argmax(f_scores)])
    print('Accuracy: ',accuracy_score(y, y_pred > thresholds[np.argmax(f_scores)]))
    return thresholds[np.argmax(f_scores)]

def save_files():
    zip_file = ZipFile(config.SAVE_PATH)
    df = pd.read_csv(zip_file.open(config.ZIP_OPEN), sep = ";")
    X = df.drop(["y"], axis = 1)
    y = df[["y"]].replace({"yes":1,"no":0})
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.05, random_state= config.RANDOM, stratify = y)
    joblib.dump((X_train, y_train), config.DATA_DIR + config.TRAIN)
    print("Saved train data")
    joblib.dump((X_test, y_test), config.DATA_DIR + config.TEST)
    print("Saved test data")