import math
import joblib
import ml_model.config as config
from ml_model.predict import run_prediction
from sklearn.metrics import accuracy_score, recall_score, precision_score


def test_run_prediction():
    # Given
    X_test, y_test = joblib.load(config.PACKAGE_ROOT + '/data/test_data.pkl')
    test_json = X_test[0:1].to_json(orient='records')
    test_json_multiple = X_test.to_json(orient='records')
    # When
    subject = run_prediction(test_json)
    subjects = run_prediction(test_json_multiple)

    # Then
    print(recall_score(subjects.get('prediction'), y_test))
    assert subject is not None
    assert subject.get('prediction')[0] == 1 or subject.get('prediction')[0] == 0
    assert subject.get('probability')[0] >= 0 and subject.get('probability')[0] <= 1

def test_run_prediction_multiple():
    # Given
    X_test, y_test = joblib.load(config.PACKAGE_ROOT + '/data/test_data.pkl')
    test_json_multiple = X_test.to_json(orient='records')
    # When
    subjects = run_prediction(test_json_multiple)
    # Then
    assert accuracy_score(subjects.get('prediction'), y_test) > 0.8
    assert recall_score(subjects.get('prediction'), y_test) > 0.4
    assert precision_score(subjects.get('prediction'), y_test) > 0.4