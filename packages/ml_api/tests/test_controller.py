import ml_model.config as config
from ml_model import __version__ as _version

import json
import math
import joblib


def test_health_endpoint_returns_200(flask_test_client):
    # When
    response = flask_test_client.get('/health')

    # Then
    assert response.status_code == 200


def test_prediction_endpoint_returns_prediction(flask_test_client):
    # Given
    # Load the test data from the regression_model package
    # This is important as it makes it harder for the test
    # data versions to get confused by not spreading it
    # across packages.
    X_test, y_test = joblib.load(config.PACKAGE_ROOT + '/data/test_data.pkl')
    post_json = X_test[0:1].to_json(orient='records')

    # When
    response = flask_test_client.post('/v1/predict/', json=post_json)

    # Then
    assert response.status_code == 200
    response_json = json.loads(response.data)
    prediction = response_json['prediction']
    response_version = response_json['version']
    assert math.ceil(prediction[0]) == 0
    assert response_version == _version