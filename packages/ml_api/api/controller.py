from flask import Blueprint, request, jsonify
from ml_model.predict import run_prediction

from api.config import get_logger

_logger = get_logger(logger_name=__name__)


prediction_app = Blueprint('prediction_app', __name__)


@prediction_app.route('/health', methods=['GET'])
def health():
    if request.method == 'GET':
        _logger.info('health status OK')
        return 'ok'


@prediction_app.route('/v1/predict/', methods=['POST'])
def predict():
    if request.method == 'POST':
        json_data = request.get_json()
        _logger.info(f'Inputs: {json_data}')

        result = run_prediction(json_data)
        _logger.info(f'Outputs: {result}')

        prediction = result.get('prediction')
        prob = result.get('probability')
        version = result.get('version')

        return jsonify({'prediction': prediction,
        				"probability": prob,
                        'version': version})